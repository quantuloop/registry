# Quantuloop Package Registry 

## Installing Python Packages

```shell
$ pip install --index-url https://gitlab.com/api/v4/projects/43029789/packages/pypi/simple <package-name>
```

**Example**:

* Installing Quantuloop Dense (quantuloop-dense):
  ```shell
  $ pip install --index-url https://gitlab.com/api/v4/projects/43029789/packages/pypi/simple quantuloop-dense
  ```
